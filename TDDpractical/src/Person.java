public class Person {

    private String name;
    private String surname;
    private String gender;
    private String emailAddress;
    private String jobTitle;
    private String phoneNumber;
    private String companyName;
    private String title;
    private String description;

    public Person() {
    }

    public Person(String name, String surname, String gender, String emailAddress, String jobTitle, String phoneNumber, String companyName, String title, String description) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.emailAddress = emailAddress;
        this.jobTitle = jobTitle;
        this.phoneNumber = phoneNumber;
        this.companyName = companyName;
        this.title = title;
        this.description = description;


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
